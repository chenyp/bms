package pers.yaoliguo.bms.service;

import java.util.List;

import pers.yaoliguo.bms.entity.SysRole;

/**
 * @ClassName:       ISysRoleService
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年7月4日        下午10:32:44
 */
public interface ISysRoleService {
	
    int deleteByPrimaryKey(String id);

    int insert(SysRole record);

    int insertSelective(SysRole record);

    SysRole selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysRole record);

    int updateByPrimaryKey(SysRole record);
    
    int selectCount(SysRole record);
    
    List<SysRole> selectAll(SysRole record);
    
    int updateByPID(SysRole record);
    
    int removeChildren(SysRole record);
    
    List<SysRole> selectRolesByKey(SysRole record);
}
