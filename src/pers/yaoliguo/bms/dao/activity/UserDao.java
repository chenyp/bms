package pers.yaoliguo.bms.dao.activity;

import org.springframework.stereotype.Repository;

import pers.yaoliguo.bms.entity.activity.User;
@Repository("UserDao")
public interface UserDao {
	 int deleteByPrimaryKey(Integer id);

	    int insert(User record);

	    int insertSelective(User record);

	    User selectByPrimaryKey(Integer id);

	    int updateByPrimaryKeySelective(User record);

	    int updateByPrimaryKey(User record);
     
}