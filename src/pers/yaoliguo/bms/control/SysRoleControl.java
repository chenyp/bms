package pers.yaoliguo.bms.control;

import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import pers.yaoliguo.bms.control.view.Message;
import pers.yaoliguo.bms.control.view.RoleView;
import pers.yaoliguo.bms.entity.SysRole;
import pers.yaoliguo.bms.service.ISysRoleService;
import pers.yaoliguo.bms.uitl.StringHelper;
import pers.yaoliguo.bms.uitl.PermissionCodeUtil;
/**
 * @ClassName:       SysRoleControl
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年7月4日        下午10:20:20
 */
@Controller
@RequestMapping("/SysRoleControl")
public class SysRoleControl extends BaseControl{
	
	@Autowired
	ISysRoleService sysRoleService;
	
	//@RequiresPermissions(value=PermissionCodeUtil.ROLE)
	@RequestMapping(value="/skipRolePage")
	public String skipRolePage(){
		
		return "redirect:/views/role/roleList.html";
	}
	
	//@RequiresPermissions(value=PermissionCodeUtil.ROLE)
	@RequestMapping("/getRoleList")
	@ResponseBody
	public Object getRoleList(RoleView roleView){
		
		Message<SysRole> msg = new  Message<SysRole>();
		
		if(roleView == null){
			roleView = new RoleView();
		}
		roleView.setDel(false);
		roleView.setPid("-1");
		roleView.setId("-1");
		List<SysRole> list = sysRoleService.selectRolesByKey(roleView);
		msg.setDataList(list);
		msg.setResult("200");
		
		return msg;
	}
	
	//@RequiresPermissions(value=PermissionCodeUtil.ROLE)
	@RequestMapping("/addRole")
	@ResponseBody
	public Object addRole(RoleView roleView){
		
		Message<RoleView> msg = new  Message<RoleView>();
		
		if(roleView == null || StringHelper.isNullOrEmpty(roleView.getRoleName())){
			msg.setResult("400");
			msg.setInfo("不能为空");
		}else{
			
			if(StringHelper.isNullOrEmpty(roleView.getPid())){
				roleView.setPid("-1");
			}
			roleView.setId(StringHelper.getUUID());
			roleView.setDel(false);
			sysRoleService.insert(roleView);
			msg.setData(roleView);
			msg.setResult("200");
			msg.setInfo("添加成功!");
		}
		
		return msg;
	}
	
	//@RequiresPermissions(value=PermissionCodeUtil.ROLE)
	@RequestMapping("/updateRole")
	@ResponseBody
	public Object updateRole(RoleView roleView){
		Message<RoleView> msg = new  Message<RoleView>();
		
		if(roleView == null || StringHelper.isNullOrEmpty(roleView.getRoleName())){
			msg.setResult("400");
			msg.setInfo("不能为空");
		}else{
			sysRoleService.updateByPrimaryKey(roleView);
			msg.setResult("200");
			msg.setInfo("修改成功!");
		}
		
		return msg;
	}
	
	//@RequiresPermissions(value=PermissionCodeUtil.ROLE)
	@RequestMapping("/deleteRole")
	@ResponseBody
	public Object deleteRole(RoleView roleView){
		Message<RoleView> msg = new  Message<RoleView>();
		
		if(roleView == null){
			msg.setResult("400");
			msg.setInfo("不能为空");
		}else{
			if(StringHelper.isNullOrEmpty(roleView.getPid())){
				roleView.setPid("-1");
			}
			roleView.setDel(true);
			sysRoleService.updateByPrimaryKeySelective(roleView);
			sysRoleService.removeChildren(roleView);
			msg.setResult("200");
			msg.setInfo("删除成功!");
		}
		
		return msg;
	}

}
