package pers.yaoliguo.bms.uitl;
import java.io.IOException;
import java.util.Date;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;


public class DateYMDHMSJsonDeserializer extends JsonDeserializer<Date> {    
       
    public Date deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {    
        try {    
            return DateUtil.formatStringToDate(jp.getText(), DateUtil.DATE_FORMAT_TIME_T);    
        } catch (Exception e) {    
            return new Date(jp.getLongValue());    
        }    
    }    
}  