package pers.yaoliguo.bms.uitl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	public static final String DATE_FORMAT_TIME_T = "yyyy-MM-dd HH:mm:ss";

	public static String formatDate(Date date, String dateFormatTimeT) {
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormatTimeT);
		 String dateString = formatter.format(date);
		return dateString;
	}

	public static Date formatStringToDate(String text, String dateFormatTimeT) throws ParseException {
		// TODO Auto-generated method stub
		
		return new SimpleDateFormat(dateFormatTimeT).parse(text);
	}

}
