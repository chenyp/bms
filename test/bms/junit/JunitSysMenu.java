package bms.junit;

import java.util.Arrays;
import java.util.List;



import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pers.yaoliguo.bms.entity.SysMenu;
import pers.yaoliguo.bms.service.impl.SysMenuService;
import pers.yaoliguo.bms.uitl.StringHelper;

/**
 * @ClassName:       JunitSysMenu
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年6月30日        下午8:53:38
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring_bean.xml"})  
public class JunitSysMenu {
	
	@Autowired
	SysMenuService sysMenuService;
	
	
	public void selectAll(){
		SysMenu menu = new SysMenu();
		menu.setDel(false);
		menu.setIcon(null);
		menu.setPid("-1");
		 
	    List<SysMenu>menus	= sysMenuService.selectAll(menu);
		System.out.println(menus.size());
		
	}
	@Test
	public void initMenu(){
		
		String sysid = StringHelper.getUUID();
		
		SysMenu menu = new SysMenu();
		SysMenu menu1 = new SysMenu();
		SysMenu menu2 = new SysMenu();
		SysMenu menu3 = new SysMenu();
		SysMenu menu4 = new SysMenu();
		SysMenu menu5 = new SysMenu();
		menu.setDel(false);
		menu.setIcon("el-icon-setting");
		menu.setId(sysid);
		menu.setMenuName("系统管理");
		menu.setPid("-1");
		menu.setSeq(1);
		menu.setState(true);
		menu.setUrl("");
		
		menu1.setDel(false);
		menu1.setIcon("el-icon-message");
		menu1.setId(StringHelper.getUUID());
		menu1.setMenuName("菜单管理");
		menu1.setPid(sysid);
		menu1.setSeq(1);
		menu1.setState(true);
		menu1.setUrl("/SysMenuControl/skipMenuPage");
		
		menu2.setDel(false);
		menu2.setIcon("el-icon-message");
		menu2.setId(StringHelper.getUUID());
		menu2.setMenuName("角色管理");
		menu2.setPid(sysid);
		menu2.setSeq(2);
		menu2.setState(true);
		menu2.setUrl("/SysRoleControl/skipRolePage");
		
		menu3.setDel(false);
		menu3.setIcon("el-icon-message");
		menu3.setId(StringHelper.getUUID());
		menu3.setMenuName("角色菜单配置");
		menu3.setPid(sysid);
		menu3.setSeq(3);
		menu3.setState(true);
		menu3.setUrl("/SysMenuRole/skipMenuRolePage");
		
		menu4.setDel(false);
		menu4.setIcon("el-icon-message");
		menu4.setId(StringHelper.getUUID());
		menu4.setMenuName("用户管理");
		menu4.setPid(sysid);
		menu4.setSeq(4);
		menu4.setState(true);
		menu4.setUrl("/UserControl/skipUserPage");
		
		menu5.setDel(false);
		menu5.setIcon("el-icon-message");
		menu5.setId(StringHelper.getUUID());
		menu5.setMenuName("组织管理");
		menu5.setPid(sysid);
		menu5.setSeq(4);
		menu5.setState(true);
		menu5.setUrl("/SysOrgControl/skiporganizPage");
		sysMenuService.insert(menu);
		sysMenuService.insert(menu1);
		sysMenuService.insert(menu2);
		sysMenuService.insert(menu3);
		sysMenuService.insert(menu4);
		sysMenuService.insert(menu5);
	}
	
	 
	public void getById(){
		SysMenu menu = sysMenuService.selectByPrimaryKey("101be989fff346b38565400125889de7");
		System.out.println(menu.getChildren().size());
		SysMenu m = new SysMenu();
		
		m.setPid("#");
		List list = sysMenuService.selectAll(m);
		System.out.println(Arrays.asList(list));
		
	}

}
